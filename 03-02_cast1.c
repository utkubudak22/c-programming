/*
	- In C prrogramming, a typecast is a way of fooling the 
	the compiler into accepting one type of variable as another type.
	- Example Typecast: c = (float)a/b
	- Info: Integer value manipulation works faster than floating point manipulation.
*/

#include <stdio.h>
#include <math.h>

int main()
{
	int a,b;
	float c;

	a = 10; b = 3;
	c = (float)a/b; // Casting the variable to floating point
	/* Question: Why did not we cast both variables a and b?
	   We can also do that, but in an other program, it might be required 
	   that those values be integers. 	
	*/
	printf("%d/%d = %.2f\n",a,b,c);

	return(0);
}


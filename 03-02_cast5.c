#include <stdio.h>

int main()
{
    int val;

	printf("Please type an integer value as an input!\n");
    scanf("%d", &val);

    printf("You typed: %.1f\n", (float)val);

	return(0);
}
/*
Ternary operators use three arguments. There is one ternary operator in C language. 
(?: -> makes if-true decisions)
*/

#include <stdio.h>

int main()
{
	int a,b,larger;

	printf("Enter a value: ");
	scanf("%d",&a);
	printf("Enter another value: ");
	scanf("%d",&b);

	larger = a > b ? a : b; // This can be used instead of the following if else block.

	/*if( a > b )
		larger = a;
	else
		larger = b;
	*/

	printf("%d is the larger value.\n",larger);

	return(0);
}


/*
    - Prompt the user to input text.
    - Send text to a function.
    - In the function, convert all letters to uppercase and
    all spaces to underlines.
    - Display the modified string.
*/

#include <stdio.h>
#include <string.h>

#define MAX_LEN 100

void convert_function(char *array);

int main(void){

    int len;
    char user_text[MAX_LEN];

    printf("Please enter a text: "),
    fgets(user_text, MAX_LEN, stdin);

    len = strlen(user_text) - 1;
    printf("Length of the text: %d\n", len);

    printf("Entered text: %s\n", user_text);

    convert_function(user_text);

    printf("Converted text: %s\n", user_text);

    return 0;

}

void convert_function(char *array){

    int len = strlen(array) - 1; 
    printf("Length of the text in function: %d\n", len);

    for(int i = 0; i < len; i++){
        if(array[0] == ' '){
            array[0] = '_';
            printf("Test");
        }
        else if(array[i] >= 97 && array[i] <= 122){
            printf("Entered text in function: %c\n", array[i]);
            array[i] = array[i] - 32;
            printf("Entered text in function: %c\n", array[i]);
        }
    }

    printf("Entered text in function: %s\n", array);

}
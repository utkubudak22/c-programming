/*
    - Sort an array of structures.
    - Create the structures with day (string) and temperature (float)
    members.
    - Order the array by temperatures, with the coldest days first.
*/

#include <stdio.h>

typedef struct Days{
    char Day[10];
    float Temperature;
}days;

int main(void){

    days weekDays[7] = {
        { "Sunday", 72.5 },
		{ "Monday", 68.4 },
		{ "Tuesday", 75.0 },
		{ "Wednesday", 73.8 },
		{ "Thursday", 65.1 },
		{ "Friday", 72.8 },
		{ "Saturday", 75.2 }
    };

    for(int outer = 0; outer < 7; outer++){
        for(int inner = outer + 1; inner < 7; inner++){
            if(weekDays[outer].Temperature > weekDays[inner].Temperature){
                days temp = weekDays[outer];
                weekDays[outer] = weekDays[inner];
                weekDays[inner] = temp;
            }
        }
    }

    for(int i = 0; i < 7; i++){
        printf("Weekday: %s, Temperature: %.1lf\n", 
                            weekDays[i].Day, weekDays[i].Temperature);
    }

    return 0;

}

# C Programming
This repository includes examples of C language from zero to hero! :)

## Table of Contents
* [Hello World](#hello-world)
* [Assignment Operators](#assignment-operators)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## Hello World
In order to compile the code, gcc compiler is used. 
Compilation is the process of converting the c source code into an executable code in chosen format.
After compiling the code, machine code is generated, which consists of 0s and 1s.
Gcc is a compiler that is installed on a lot of UNIX-like operating systems.

You can compile the source code and execute it with following commands:
```
In order to compile the source code: gcc -o hello_world hello_world.c
In order to compile multiple source codes: gcc -o output source1.c source2.c source3.c
In order to compile the source code with a specific C version: gcc -std=c11 -pedantic foo.c
In order to compile the sorce code with all warnings: gcc -Wall -Wextra -std=c2x -pedantic foo.c
In order to execute the compiled code: ./hello_world
```
The -o means output of this file.




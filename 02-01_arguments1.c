/*
*argv[] -> array of pointers
**argv -> pointer to a pointer
*/

#include <stdio.h>

/*
char *array_pointer[] = {"utku", "ege", "sibel", "sahin"}; -> This creates an array of strings.
array_pointer[0] -> "utku"
array_pointer[1] -> "ege"
array_pointer[2] -> "sibel"
array_pointer[3] -> "sahin"
*/

int main(int argc, char *argv[])
{
	printf("There were %d command line arguments\n",argc); // The argc shows the number of command line arguments, which is 1 in this case -> name of the program.
	printf("This program is named %s\n", argv[0]); //The argv[0] shows the name of the compiled program.
	printf("This program is named %s\n", argv[1]);

	return(0);
}


/*
    This code takes a string from the user as an input,
    and then sorts the input as a character array.
*/

#include <stdio.h>
#include <string.h>

#define MAX_LEN 100

int main(void){

    int len;

    char user_text[MAX_LEN];

    printf("Please enter a text: "),
    // scanf("%s", user_text); -> strlen only reads the first word, use fgets.
    fgets(user_text, MAX_LEN, stdin);

    len = strlen(user_text);
    printf("Length of the text: %d\n", len);

    printf("Unsorted character array: %s\n", user_text);

    for(int outer = 0; outer < len; outer++){
        for(int inner = outer + 1; inner < len; inner++){
            if(user_text[outer] > user_text[inner]){
                int temp = user_text[outer];
                user_text[outer] = user_text[inner];
                user_text[inner] = temp;
            }
            printf("Sorting character array: %s\n", user_text);
        }
    }

    printf("Sorted character array: %s\n", user_text);

    return 0;

}
#include <stdio.h>

int main()
{
	int a[10],*ptra;

	ptra = a;
	printf("Address of a = %p\n",a + 1);
	printf("Address of a = %p\n",&a + 1); 
	// Because &a holds the whole memory space of the array,
	// so even if we increase it by one, it will increase the address by
	// (len(int) = 4) * 10
	printf("Pointer ptra = %p\n",ptra);

	return(0);
}


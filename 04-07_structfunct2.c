#include <stdio.h>
#include <string.h>

typedef struct person {
	char name[32];
	int age;
	float iq;
}person;

// Returning to a struct means high overhead, thats why use pointers and return to a address!
struct person fetchStruct(void); // This function returns to a struct.
void showStruct(person p);

int main()
{
	showStruct(fetchStruct());
	return(0);
}

struct person fetchStruct(void)
{
	static struct person author;

	strcpy(author.name,"Utku");
	author.age = 23;
	author.iq = 280;

	return(author);
}

void showStruct(person p)
{
	printf("Author %s is %d years old\n",
			p.name,
			p.age);
	printf("%s has an IQ of %.1f\n",
			p.name,
			p.iq);
}


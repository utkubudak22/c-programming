#include <stdio.h>

// This function returns to a char pointer.
// SO we can return to an array with a function.
char *repeat(char r)
{
	int x;
	/*
		If we dont use static keyword here, even though we return to the
		address of the data, the address is lost after the function is executed.
		This will lead to a warning : function returns address of local variable
		And during run time: Segmentation Fault, because the pointer shows nowhere: NULL
	*/
	static char string[32];

	for(x=0;x<32;x++)
		string[x] = r;

	return string;
}

int main()
{
	char c;

	printf("Type a character: ");
	scanf("%c",&c);
	printf("%s\n",repeat(c));

	return(0);
}


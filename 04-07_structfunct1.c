#include <stdio.h>
#include <string.h>

typedef struct person {
		char name[32];
		int age;
		float iq;
	}person;

void showStruct(person p);

int main()
{
	/* The structure must defined globally, if it is defined locally
	   in this functio, the function declaration/definition will not see
	   the structure and this will lead to an error. */
	/*struct person {
		char name[32];
		int age;
		float iq;
	};*/
	struct person author;

	strcpy(author.name,"Utku Budak");
	author.age = 23;
	author.iq = 280;

	showStruct(author);

	return(0);
}

void showStruct(person p)
{
	printf("Author %s is %d years old\n",
			p.name,
			p.age);
	printf("%s has an IQ of %.1f\n",
			p.name,
			p.iq);
}


/*
    - Code with at least two function, one to fill a structure
    with data and another to displa the structures data.
    - Have the structures members include name and age.
    - Prompt the user to input their name and age and display the results.
*/

#include <stdio.h>
#include <string.h>

typedef struct person{
    char name[40];
    int age;
}person;

void fillStructure(person *Person, char *name, int age);

void displayStructure(person Person);

int main(void){

    person Person;

    fillStructure(&Person, "Utku Budak", 23);
    displayStructure(Person);

    return 0;

}

void fillStructure(person *Person, char *name, int age){

    int len = strlen(name);

    memcpy(Person -> name, name, len + 1);
    Person -> age = age;

    printf("Person %s is %d years old\n",
			Person -> name,
			Person -> age);

}

void displayStructure(person Person){

    printf("Person %s is %d years old\n",
			Person.name,
			Person.age);

}
/*
	- Variables are local to their functions.
	- Values are used only when the function is called.
	- Values and variables are lost after the function runs.
	- Two ways to retain values: 
		- Define as global variables.
		- Use the keyword static.
*/

#include <stdio.h>

void f(void)
{
	static int x = 0; // The x variable will be assigned to 0 just once and keep its value each time the function is called.

	printf("Value of x is %d\n",x);
	x++;
	printf("Value of x is %d\n",x);
}

int main()
{
	f();
	f();

	return(0);
}


/*
	- Use the command "gcc 03-02_cast2.c -lm" in order to compile the code.
	- Because the header contains only the declaration of the function, 
	the definition of the function should be linked during the compilation/link time.
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
	int a;
	float aroot;

	printf("Type an integer: ");
	scanf("%d",&a);
	aroot = sqrt(abs(a));
	printf("The square root of %d is %f\n",abs(a),aroot);

	return(0);
}


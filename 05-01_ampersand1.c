/*
	- '&' -> Ampersand Operator.
	- Ampersand operator returns the memory location of a variable.
	- Does not have to be initialized.
	- %p placeholder is is used to print address of a variable which is
	the result of the '&'clear operator.
*/

#include <stdio.h>

int main()
{
	char a;
	double b;
	float c;
	int d;

	printf("Address of char variable 'a':\t%p\n",&a);
	printf("Address of double variable 'b':\t%p\n",&b);
	printf("Address of float variable 'c':\t%p\n",&c);
	printf("Address of int variable 'd':\t%p\n",&d);

	return(0);
}


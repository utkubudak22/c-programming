/*
    - Create an integer and integer pointer variables.
    - Using the pointer, assign a value to the integer and
    display the result.
    - Using the pointer. increment the integer's value and
    display the result.
*/

#include <stdio.h>

int main(void){

    int val, *ptr_val;

    ptr_val = &val;

    *ptr_val = 5;

    printf("Value of the val: %d\n", val);

    ++*ptr_val;

    printf("Incremented value of the val: %d\n", val);

    return 0;

}
#include <stdio.h>

int *function_array(void){

    static int array_val[] = {2, 3, 5, 7, 9};

    return array_val;

}

int main(void){

    printf("The array values are:\n");

    for(int i = 0; i < 5; i++){
        printf("%d\t", *(function_array() + i));
    }

    printf("\n");

    return 0;

}
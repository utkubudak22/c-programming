/*
    - Create a character array - a string.
    - Obtain the string's length.
    - Display each character by using pointer math.
    (not array notation) 
    - Do not change the pointer's base address.
*/

#include <stdio.h>
#include <string.h> 

int main(void){

    char char_array[] = "Utku Budak";

    char *ptr = char_array;

    int length = strlen(char_array);
    printf("String Length: %d\n", length);

    for(int i = 0; i < length; i++){
        printf("Character[%d]: %c\n", i, *(ptr+i));
    }

    return 0;

}
/*
	- Order of presedence: It determines which operations
	take place first.
*/

#include <stdio.h>

int main()
{
	int a;

	a = 5 + 20 * 2 - 8 / 2;
	printf("The answer is %d\n",a); // Expected: 41

	return(0);
}

